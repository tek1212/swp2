# Shows how simple string comparisons is in Python
# mal@tris8VM 2020-11-11

passw = input('Enter the password: ')

if passw == 'password123':				# String comparisons are easy in Python
	print('Password accepted.')
else:
	print('Wrong password.')
