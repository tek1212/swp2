# Calculate Shipping charges based on weight
# mal@tris8VM 2020-11-11

weight = float(input('Enter the items weight (in pounds): '))
perpounds = 0

if weight <= 0:
	perpounds = 0
elif weight > 0 and weight <= 2.0:
	perpounds = 1.10
elif weight > 2.0 and weight <= 6.0:
	perpounds = 2.20
elif weight > 6.0 and weight <= 10.0:
	perpounds = 3.70
else: #weight > 10.0
	perpounds = 3.80

print('The shipping price is $', format(weight * perpounds, ',.2f'), sep='' )
