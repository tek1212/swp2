# A Date is magical if the multiplication of month and day, equals 2-digit year.
# mal@tris8VM 2020-11-11

def main():
	print('A Date is magical if the multiplication of month and day, equals 2-digit year.')
	print('Example: 7/10/70')
	month = int(input('Enter month: '))
	day = int(input('Enter day: '))
	year = int(input('Enter 2-digit year: '))

	if magicdate(month, day, year):
		print('Congratulation, the date ', month,'/',day,'/',year, ' is magical.', sep='')
	else:
		print('Sorry, the date ', month,'/',day,'/',year, ' is not magical.', sep='')

def magicdate(month, day, year):
	if month * day == year:
		return True
	else:
		return False

main()
