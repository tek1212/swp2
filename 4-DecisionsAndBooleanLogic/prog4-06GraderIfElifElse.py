# Shows how nested if-else in Python
# mal@tris8VM 2020-11-11

A_SCORE = 90
B_SCORE = 80
C_SCORE = 70
D_SCORE = 60

def main():
	score = float(input('Enter the test score: '))
	
	if score >= A_SCORE:
		print('Your grade is A.')
	elif score >= B_SCORE:
		print('Your grade is B.')
	elif score >= C_SCORE:
		print('Your grade is C.')
	elif score >= D_SCORE:
		print('Your grade is D.')
	else:
		print('Your grade is F.')

# Call the main()
main()
