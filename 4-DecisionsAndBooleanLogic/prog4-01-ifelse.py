# Shows a simple if-else statement
# mal@tris8VM 2020-11-11
# updated @kiddW7 2022-10-24

def main():
	temp = float(input('Enter the current temperature (in Celcius): '))
	
	if temp <= 30 and temp >=20:
		print('Nice weather we\'re having.')
	elif temp <20:
		print('A little cold, isn\'t it?')
	else: # temp > 30
		print('A little hot, isn\'t it?')
		
main()
