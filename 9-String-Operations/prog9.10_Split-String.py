#tom@W7 2023-01-23 Monday
#Describes and tests on string.split()

st0 = '2023 01 27'
st1 = '2023-01-28'
st2 = '2023---01---29'
st3 = '2023---01---31'
st4 = '2023---01---32'

print()
print("'", st0, "'" ,".split() = ", st0.split(), sep='')
print("'", st1, "'" ,".split('-') = ", st1.split('-'), sep='')
print("'", st2, "'" ,".split('---') = ", st2.split('---'), sep='')
print("'", st3, "'" ,".split('-') = ", st3.split('-'), sep='')
print("'", st4, "'" ,".split('--') = ", st4.split('--'), sep='')

print()
print("As in the st3 example, the string.split() method correctly separates blanks '' into blanks.")

print()
print("Even in un-even separators like example st4, it would correctly separate the remaining (odd) separator char.")
print("The implementation is generally correct.")