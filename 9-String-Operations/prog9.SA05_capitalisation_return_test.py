#@tom@W7 2023-01-25 Wednesday
#Test string capitalization effects on the original string and its return value

name = 'joe'

print('name.lower() =', name.lower())
print('name.upper() =' , name.upper())
print('name =', name)