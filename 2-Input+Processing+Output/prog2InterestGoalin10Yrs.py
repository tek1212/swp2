future = float(input('How much money you want to have in 10 years? '))

interest = float(input('What is your current yearly interest rate? (hint: Enter 0.08 for 8%) '));

principal = future / (1.0 + interest) ** 10

print('So save', format(principal, '9,.2f'), 'amount of money now! ;))')
