principal = float(input('How much money you hv in your savings? '))

interest = float(input('What\'s the annual interest rate? (hint: Enter 0.08 for 8%)  '))

ten_yrs_later = principal * (1.0 + interest) ** 10

print('In 10 yrs time, you will have', format(ten_yrs_later, '9,.2f'), 'of money. Congratulations!')
