principal = float(input('How much money you have in your savings now? '))

rate = float(input('What is the interest rate per period? (hint: Enter 0.08 for 8%) '))

period = float(input('How long (the amount of period multiplier) you want to save the money for? '))

later = principal * (1.0 + rate) ** period

print ('In', period, ' period amount of time, you\'ll have', format(later, '9,.2f'), 'amount of money.')
