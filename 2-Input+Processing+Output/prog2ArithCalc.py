print('Addition: 3 + 5 =', 3 + 5)
print()

print('Subtraction: 13 - 8 =', 13 - 8)
print()

print('Multiplication: 13.0 * 2 =', 13.0 * 2)
print()

print('Multiplication: 13 * 2 =', 13 * 2)
print()

print('# In Reg Division, any division is exact-ly as it-is.. INTs or FLOATs..\
 and everything is turned to FLOATS')
print()

print('Division: 30 / 6 =', 30 / 6)
print()

print('Division: 32 / 6 =', 32 / 6)
print()

print('# In Int Division, any division is floored. INTs or FLOATs..')
print()

print('Int Division: 27 // 5.0 =', 27 // 5.0)
print()

print('Int Division: 27.5 // 5 =', 27.5 // 5)
print()

print('Int Division: 27 // 5 =', 27 // 5)
print()

print('Remainder: 17 % 5 =', 17 % 5)
print()

print('Exponentiation: 2 ** 4 =', 2 ** 4)
print('Exponentiation: 2 ** 7 =', 2 ** 7)
print()

print('Don\'t know what this is: 2 ^ 4 =', 2^4, '...Likely a XOR??!? No? loL')
