print('This is a demo of data-types:\n')

print('type(1)')
print(type(1))
print()

print('type(1.0)')
print(type(1.0))
print()

print("type('Jane Austen')")
print(type('Jane Austen'))
print()

room = 728
dollars = 19.28
name = 'Bob Austen'

print('type(room)')
print(type(room))
print('type(dollars)')
print(type(dollars))
print('type(name)')
print(type(name))
