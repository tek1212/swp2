#Print Showdown

#Prog 2.1
print('Kate Austen') #Name
print('123 Beach Drive') #Address
print('Sexy Beach, Carolina 12345') #State

print() # only python3 compatible

#Prog 2.4
print('Your assignment is to read "Hamlet" by tomorrow')

print('') # python2 and 3 ok...

#Prog 2.3
print("Don't fear..")
print("I'm here!")

print()

#Prog 2.4
print("""I'm reading "Hamlet" tonight.""")

print()

print("""This
"Spans"
Mulitple Lines...""")

print()

print('#---Section 2.8 More bout Data Output: Formatting, Separators, Line Ending---\n')

print('AAA','BBB','CCC', sep='')
print('BBB','BBB','CCC', sep=' ')
print('BBB','BBB','CCC', sep='*')

print()

print('111','222','333', sep='||',end='End\n')

print()

print('444','555','666', sep='*_*', end='*_0_*\n')

print()

#Bye
