# mal@tris8VM	2020-11-12

import random

print('This program produces random number.')

again = 'y'

while again.lower() == 'y':
	min = int(input('Enter the minimum for the range: '))
	max = int(input('Enter the maximum for the range: '))

	print('The number is', random.randint(min,max))
	print()

	again = input('Get another random number (y = yes) : ')
