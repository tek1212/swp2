# mal@tris8VM	2020-11-13 Friday

import random

print('This is a Math Quiz Game.')
print('You have to add 2 3-digit numbers correctly.')
print()

play_again = 'y'

while play_again == 'y':
	
	rand1 = random.randint(100,999)
	rand2 = random.randint(100,999)
	
	print(rand1)
	print(rand2, '+')
	
	answer = int(input())
	
	if answer == rand1 + rand2:
		print('Congratulations, you are correct!')
	else:
		print('Sorry, wrong answer.', rand1, '+', rand2, '=', rand1+rand2)
	
	play_again = input('Play again (y = yes): ')
	print()
