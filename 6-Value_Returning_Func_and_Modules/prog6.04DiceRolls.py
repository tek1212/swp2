# Program that simulates dice rolls
# mal@tris8VM	2020-11-12

import random

MIN = 1
MAX = 6

def main():
	again = 'y'
	
	while again.lower() == 'y':
		print('Rolling the dices...')
		print('Their values are:')
		
		print(random.randint(MIN,MAX))
		print(random.randint(MIN,MAX))
		print()
		
		#Do another roll ?
		again = input('Roll them again? (y = yes): ')

main()
