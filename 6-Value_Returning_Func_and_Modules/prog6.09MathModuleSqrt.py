# Shows the math.sqrt() function
# mal@tris8VM	2020-11-12

import math

number = float(input('Enter a number to be square-rooted: '))

print('The square root of', number, '=', math.sqrt(number)) #shows the math.sqrt()
