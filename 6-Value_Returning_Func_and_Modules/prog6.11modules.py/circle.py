# A module for calculating a circle's area and circumference
# mal@tris8VM	2020-11-12 Friday

import math

# Returns a circle's area
def area(radius):
	return math.pi * radius**2

# Returns a circle's circumference
def circumference(radius):
	return 2 * math.pi * radius
