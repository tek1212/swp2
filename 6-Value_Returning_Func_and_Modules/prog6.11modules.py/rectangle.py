# A module for rectangle area and perimeter calculation
# mal@tris8VM	2020-11-13 Friday

# Returns rectangle's area
def area(width, length):
	return width * length

# Returns rectangle's perimeter
def perimeter(width, length):
	return 2 * (width + length)
