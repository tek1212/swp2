# This program allows user to calculate areas and perimeters of circles and rectangles...
# mal@tris8VM	2020-11-13 Friday

import circle
import rectangle

CIRCLE_AREA_CHOICE = 1
CIRCLE_CIR_CHOICE = 2
RECTANGLE_AREA_CHOICE = 3
RECTANGLE_PERI_CHOICE = 4
QUIT_CHOICE = 5

def display_menu():
	print('			MENU')
	print('1) Area of a circle')
	print('2) Circumference of a circle')
	print('3) Area of a rectangle')
	print('4) Perimeter of a rectangle')
	print('5) Quit')

def main():
	choice = 0
	
	while choice != QUIT_CHOICE:
		print()
		display_menu()
		
		choice = int(input('Enter your choice: '))
		
		if choice == CIRCLE_AREA_CHOICE:
			radius = float(input("Enter the circle's radius: "))
			print('The area is', circle.area(radius))
		elif choice == CIRCLE_CIR_CHOICE:
			radius = float(input("Enter the circle's radius: "))
			print('The circumference is', circle.circumference(radius))
		elif choice == RECTANGLE_AREA_CHOICE:
			width = float(input("Enter the rectangle's width: "))
			length = float(input("Enter the rectangle's length: "))
			print('The area is', rectangle.area(width, length))
		elif choice == RECTANGLE_PERI_CHOICE:
			width = float(input("Enter the rectangle's width: "))
			length = float(input("Enter the rectangle's length: "))
			print('The perimeter is', rectangle.perimeter(width, length))
		elif choice == QUIT_CHOICE:
			print('Exiting...')
		else:
			print('Error: Invalid Selection.')


main()
