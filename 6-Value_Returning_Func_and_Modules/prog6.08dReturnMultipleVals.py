# Shows how a function can return multiple values
# mal@tris8VM	2020-11-12

def main():
	firstname, lastname = get_name()
	print('Hello', firstname, lastname)

def get_name():
	first = input('Enter your first name: ')
	last = input('Enter your last name: ')
	
	return first, last

main()
