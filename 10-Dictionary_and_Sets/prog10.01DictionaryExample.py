#@alanW10 2023-06-29 Thurs

def main():
	phonebook = {}
	printbook(phonebook)

	inp = 0

	while inp != 'q':
		display_choices()

		inp = input('Enter choice: ')
		if inp == 'a':
			add(phonebook)
		elif inp == 'r':
			remove(phonebook)

		printbook(phonebook)
		print()

def printbook(book):
	print('Contents of phonebook is now: ')
	for k, v in book.items():
		print(k, v)

def display_choices():
	print("Enter 'a' if you want to add")
	print("Enter 'r' if you want to remove")
	print("Enter 'q' if you want to quit")

def add(book):
	name = input("Enter the name to add: ")
	number = input("Enter the number to add: ")

	book[name] = number

def remove(book):
	name = input("Enter the name to remove: ")

	print(book.pop(name, "The name to remove is not found."))

main()