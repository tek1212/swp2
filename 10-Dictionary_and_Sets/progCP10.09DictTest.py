#@alanW10 2023-07-04 Tues

stuff = {111 : 'aaa', 222 : 'bbb', 333 : 'ccc'}

for k in stuff:
	print(k)

for v in stuff.values():
	print(v)

for k, v in stuff: #error
	print(v)