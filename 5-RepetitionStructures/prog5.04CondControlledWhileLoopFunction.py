# Shows function call in a condition controlled while loop
# mal@tris8VM 2020-11-12

def helloname():
	name = input('What\'s your name? ')
	print('Hello', name, 'nice to know you!')

keep_goin = 'y'

while keep_goin.lower() == 'y' or keep_goin.lower() == 'yes':
	helloname()
	keep_goin = input('Do you want to enter another name (Enter y for yes)? ')
	print()
