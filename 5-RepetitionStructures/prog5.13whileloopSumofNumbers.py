print('This program calculates the sums of numbers entered using while loop. Enter 0 to terminate')

total = 0 #initiates the accumulator to 0 (recomended)

init = True
number = 0
while init or number != 0:
	number = float(input('Enter a number: '))
	total += number
	init = False

print('The total is', total)

## The init var is my hack and later imo is a really bad and buggy implementation
## Check out the prog5.14(do)WhileLoopSumOfNumbers for example with more proper initialization read
