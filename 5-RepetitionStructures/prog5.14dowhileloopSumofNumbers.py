print('This program calculates the sums of numbers entered using do-while loop. Enter 0 to terminate')

total = 0 #initiates the accumulator to 0 (recomended)

number = float(input('Enter a number: '))
while number != 0:
	total += number
	number = float(input('Enter a number: '))

print('The total is', total)
