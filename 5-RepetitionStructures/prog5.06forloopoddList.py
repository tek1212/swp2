# Shows simple for loop (using a list of odd numbers)
# mal@tris8VM	2020-11-12

for num in [1, 3, 5, 7, 9]:
	print(num)
