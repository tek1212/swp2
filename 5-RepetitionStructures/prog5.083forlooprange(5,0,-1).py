# Shows simple for loop (using ranges)
# mal@tris8VM	2020-11-12

for num in range(5, 0, -1):
	print(num)
