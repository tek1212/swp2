# Shows how to build Triangular Pattern of Asterisk using for loop
# mal@tris8VM	2020-11-12

rows = int(input('How many rows? '))

for r in range(rows):
	for c in range(r+1):
		print('*', end='')
	print()
