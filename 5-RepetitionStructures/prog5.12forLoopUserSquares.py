# Shows for loop that can be controlled by user input
# mal@tris8VM	2020-11-12

print('This program displays a list of numbers and their squares.')

start = int(input('Enter the starting number (integer): '))
end = int(input('Enter the ending number (integer): '))

print('Numbers\tSquares')
print('-----------------')

for no in range(start, end + 1):
	print(no, '\t', no**2)
