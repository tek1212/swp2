# Shows condition controlled while loop
# mal@tris8VM 2020-11-11

keep_goin = 'y'

while keep_goin.lower() == 'y' or keep_goin.lower() == 'yes':
	name = input('What\'s your name? ')
	print('Hello', name, 'nice to know you!')
	
	keep_goin = input('Do you want to enter another name (Enter y for yes)? ')
	print()
