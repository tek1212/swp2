# Inverted Triangle Pattern Right Justified using for loop
# @kiddW7	2022-11-08

rows = int(input('How many rows? '))

for r in range(rows, 0, -1):
	for s in range(rows-r):
		print(' ', end='')
	for c in range(r):
		print('*', end='')
	print()
