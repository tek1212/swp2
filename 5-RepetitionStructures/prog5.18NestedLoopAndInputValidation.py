# Shows nested loop to calculate students' test scores
# mal@tris8VM	2020-11-12

num_students = int(input('How many students do you have? '))
num_scores = int(input('How many tests do you have? '))

for student in range(num_students):	#outer loop
	total_score = 0.0
	
	print('Student number', student+1)
	print('-----------------')
	
	for test in range(num_scores):	#inner loop
		print('Test number', test+1, end='')
		score = float(input(': '))		#priming read for input validation loop
		while score < 0 or score > 100:	#input validation loop
			print('Invalid Input: The score cannot be negative or greater than 100')
			print('Test number', test+1, end='')
			score = float(input(': '))
		
		total_score += score
	
	average = total_score / num_scores
	
	print('The average for student', student+1, 'is: ', average)
	print()
