# Shows how to build Pattern of Stair Steps using for loop
# mal@tris8VM	2020-11-12

stairs = int(input('How many steps of stairs? '))

for r in range(stairs):
	for c in range(r):
		print(' ',end='')
	print('---')
