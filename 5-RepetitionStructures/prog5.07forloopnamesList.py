# Shows simple for loop (using a list of odd numbers)
# mal@tris8VM	2020-11-12

for name in ['Winken', 'Blinken', 'Nod', 'James']:
	print(name)
