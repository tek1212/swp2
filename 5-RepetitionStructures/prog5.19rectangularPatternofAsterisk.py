# Shows how to build Rectangular Patterns of Asterisk using for loop
# mal@tris8VM	2020-11-12

rows = int(input('How many rows? '))
cols = int(input('How many columns? '))

for r in range(rows):
	for c in range(cols):
		print('*', end='')
	print()
