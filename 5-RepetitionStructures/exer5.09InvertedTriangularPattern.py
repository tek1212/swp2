# Inverted Triangle Pattern using for loop
# mal@tris8VM	2020-11-12

rows = int(input('How many rows? '))

for r in range(rows, 0, -1):
	for c in range(r):
		print('*', end='')
	print()
