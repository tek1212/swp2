# Shows simple for loop (using a list of numbers)
# mal@tris8VM	2020-11-12

for num in [1, 2, 3, 4, 5]:
	print(num)
