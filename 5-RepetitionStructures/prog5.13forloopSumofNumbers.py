print('This program calculates the sums of numbers entered using for loop (using ranges)')
max = int(input('How many numbers do you want to add ? '))

total = 0 #initiates the accumulator to 0 (recomended)

for i in range(max):
	number = float(input('Enter the number: '))
	total += number

print('The total is', total)
