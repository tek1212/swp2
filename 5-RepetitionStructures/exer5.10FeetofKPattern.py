# Shows how to build Pattern of 'Feet of K'
# mal@tris8VM	2020-11-12

stairs = int(input('How long of feet of K? '))

for r in range(stairs):
	print('#', end='')
	for c in range(r):
		print(' ',end='')
	print('#')
