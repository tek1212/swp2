# Shows how to use keyword args
# mal@tris8VM 2020-11-10

def main():
	prin = float(input('Pls enter your principal: '))
	rate = float(input('Pls enter the rate: '))
	period = float(input('Pls enter the period: '))
	show_interest(rate=rate, periods=period, principal=prin)

def show_interest(principal, rate, periods):
	interest = principal * rate * periods
	print('The interest will be Rp', format(interest, ',.2f'))

main()
