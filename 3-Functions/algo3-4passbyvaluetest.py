# Shows that arguments are passed by reference
# mal@tris8VM

def main():
	x = 2
	y = 3.4
	print('In main:', x, y)
	changefunc(x, y)
	print('In main:', x, y)

def changefunc(a, b):
	a = 0
	b = 0
	print('In change func', a, b)

main()
