# Shows that local variables are local
# mal@tris8VM 2020-11-10

def main():
	#Call texas func.
	texas()
	#Call california func.
	california()

def texas():
	birds = 5000
	print('texas has', birds, 'birds.')

def california():
	birds = 8000
	print('california has', birds, 'birds.')

main()
