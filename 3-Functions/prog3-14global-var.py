# Shows how to use global variable
# mal@tris8VM 2020-11-10

NUMBER = 17

def main():
	print('Global Number is', NUMBER)
	shownumber()
	changenumber()
	print('Global Number is', NUMBER)
	shownumber()

def shownumber():
	print('shownumber: Global Number is', NUMBER)

def changenumber():
	global NUMBER		#to CHANGE a global vars, you hv to declare it 'global' in a function
	NUMBER = 18			#otherwise it's only a local var

main()
