# Created by mal@tris8VM 2020-11-10
# Sample of Simple Functions

def introduction():
	print('Hi')
	print('I am Arthur Baratheon')
	print()

def content():
	print('I am a stupid king, who only cares about my interests')
	print('I will rule forever and ever and everr...')
	print()

def closing():
	print('The police, the house and the senate, and the rich are all on my side')
	print('Dont effin\' mess with me!')
	print()

def main():
	introduction()
	content()
	closing()

main()
