# Shows how to use arguments to function
# mal@tris8VM 2020-11-10

def sum(num1, num2):
	result = num1 + num2
	print(result)

def main():
	number1 = float(input('Pls enter 1 number: '))
	number2 = float(input('Pls enter another number: '))
	
	print('The sum of', number1, 'and', number2, 'is:')
	sum(number1, number2)

main()
