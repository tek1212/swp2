# Instruction for dryer diassembly (wait for user input)
# mal@tris8VM 2020-11-10

def waitforinput():
	input('Press Enter to see the next step.')

def startup():
	print('This program tells you how to disassemble an ACME laundry dryer.')
	print('There are 4 steps in the process.')
	print()

def step1():
	print('Step 1: Unplug the dryer and move it away from the wall.')
	print()

def step2():
	print('Step 2: Remove the 6 screws from the back of the dryer.')
	print()

def step3():
	print('Step 3: Remove the back panel.')
	print()

def step4():
	print('Step 4: Pull the top of the dryer straightup.')

def main():
	startup()
	waitforinput()
	step1()
	waitforinput()
	step2()
	waitforinput()
	step3()
	waitforinput()
	step4()

main()
