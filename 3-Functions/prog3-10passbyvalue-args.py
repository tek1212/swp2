# Shows args are pass-by-value
# mal@tris8VM 2020-11-10

def main():
	value = 99
	print('The value is', value)
	change(value)
	print('Back in main the value is', value)

def change(arg):
	arg = 5
	print('Now in change func. the value is', arg)

main()
