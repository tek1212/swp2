# This program shows how to make a copy of a list
# mal@tris8VM	2020-11-17 Tuesday

list1 = [1,2,3,4]
list2 = list1

print('list1 = [1,2,3,4]')
print('list2 = list1')
print()

print('#this will not copy content list1 to a new list2.')
print('#instead it will only make list2 reference the same list as list1.')
print()

list1[0] = 99
print('list1[0] = 99')
print('list1 =', list1)
print('list2 =', list2)
print()

print('***If you wish to make a copy of the list, you hv do either of these ways:')
print()
print('1) Iteratively copy (append more exactly) each contents in a loop:')
print()
list3 = []
for i in list1:
	list3.append(i)

print('''list3 = []
for i in list1:
	list3.append(i)''')

print()
list3[0] = 66
print('list3[0] = 66')
print('list1 =', list1)
print('list3 =', list3)
print()

print('2) Or use concatenation operator, to concatenate empty list with the orig list:')
list4 = [] + list1
print('list4 = [] + list1')
list4[0] = 77
print('list4[0] = 77')
print('list1 =', list1)
print('list4 =', list4)

