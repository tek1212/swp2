# Shows the 'in' List operator
# mal@tris8VM	2020-11-14 Saturday

prod_nums = ['V122','V123','V125','V126','P133','P125']

print('prod_nums =', prod_nums)

search = input('Enter product number: ')

# Determine if searched product number is in the list
if search in prod_nums:
	print (search, 'was found in the list.')
else:
	print (search, 'was not found in the list.')
