# Shows Operations that can be done on List
# mal@tris8VM	2020-11-14 Saturday

odd_list = [1,3,5,7,9]
print('odd_list =', odd_list)
print()

names_list = ['Molly','Steven','Will','Andy','Fred']
print('names_list =', names_list)
print()

info_list = ['Alicia',27,1550.67]
print('info_list =', info_list)
print()

print('List can be multiplied (repeated): odd_list * 3 =')
print(odd_list * 3)
print()

print('List can be made from iterable:')
print('iter_list = list(range(1,10,2))')
iter_list = list(range(1,10,2))
print('print(iter_list) =', iter_list)
print()

print('List can be indexed like arrays in C:')
print('print(odd_list[0], odd_list[1], odd_list[2], odd_list[3], odd_list[4])')
print(odd_list[0], odd_list[1], odd_list[2], odd_list[3], odd_list[4])
print()

print('len(odd_list) =', len(odd_list))
print()

print('List are mutable:')
print('odd_list[0] = 99')
odd_list[0] = 99
print('print(odd_list) =', odd_list)
print()

print('List can be concatenated with + operator:')
mixed_list = odd_list + names_list
print('mixed_list = odd_list + names_list')
print('print(mixed_list) =', mixed_list)
print()

print('List can be sliced:')
slice_list = [0,1,2,3,4,5,6,7,8,9]
print('slice_list =', slice_list)
print('slice_list[2:5] = ', slice_list[2:5])
print('slice_list[:5] = ', slice_list[:5])
print('slice_list[2:] = ', slice_list[2:])
print('slice_list[-5:] = ', slice_list[-5:])
print('slice_list[::2] = ', slice_list[::2])
print('slice_list[::-2] = ', slice_list[::-2])
print('slice_list[::3] = ', slice_list[::3])
print()
