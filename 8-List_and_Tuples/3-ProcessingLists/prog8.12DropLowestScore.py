# Get the scores of exams, drop the lowest score and calculate its average (A slightly modified example using list methods)
# kidd@kiddW7 2022-12-27 Tuesday ==IChonk Died

def main():
	scores_list = get_scores()
	
	print('\nThe scores are: ' , scores_list)
	
	min_score = min(scores_list)
	
	if len(scores_list) >1:
		try:													#This imo is useless try catch
			scores_list.remove(min_score)
		except ValueError:
			pinrt('Cant remove min score')
	
	print('\nThe refined scores are: ' , scores_list)
	
	average(scores_list)
	
def get_scores():
	scores = [ ]
	cmd = 'y'
	
	while cmd == 'y':
		value = float(input('Input the score: '))
		scores.append(value)
		
		cmd = input('\nDo you wanna input another score? (y for yes)')
			
	return scores

def average(scores_list):
	total = 0.0
	
	for value in scores_list:
		total += value
	
	avg = total / len(scores_list)
	
	print('\nThe avg score is : ', avg)

main()
