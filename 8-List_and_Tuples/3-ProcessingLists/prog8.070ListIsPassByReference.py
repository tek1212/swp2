# Show that list arguments are pass-by-reference
# mal@tris8VM	2020-11-18 Wednesday

def main():
	list = [1,2,3]
	print('in main(): list =', list)
	func(list)
	print('in main(): list =', list)

def func(list):
	list[1] = 99
	print('in main().func(): list =', list)

main()
