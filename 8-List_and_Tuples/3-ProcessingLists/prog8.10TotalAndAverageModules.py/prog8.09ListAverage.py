# Shows an example of list averaging function
# mal@tris8VM	2020-11-18 Wed
import prog8_10ListTotalFunc

def get_list():
	max_list = int(input('How many numbers do you wish to enter: '))
	list = []
	
	for i in range(max_list):
		query = 'Enter score number ' + str(i+1) + ' : '
		score = float(input(query))
		list.append(score)
	
	return list

def main():
	list = get_list()
	total = prog8_10ListTotalFunc.get_total(list)

	print('List total =', total)
	print('The average is =', total / len(list))

main()
