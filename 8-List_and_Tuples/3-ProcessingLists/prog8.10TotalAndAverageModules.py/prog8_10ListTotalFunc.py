# A list total function
# mal@tris8VM	2020-11-18 Wednesday

def get_total(list):
	total = 0.0
	
	for j in list:
		total += j
	
	return total

def main():
	list = [1,2,3,4,5]
	print('The total of the sample list is', get_total(list))

# main()
