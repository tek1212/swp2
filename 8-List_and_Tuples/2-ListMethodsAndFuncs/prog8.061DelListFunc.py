# Shows the del list[index] function
# mal@tris8VM	2020-11-16 Monday

list = [0,1,2,3,4,5]
print('list =', list)
del list[4]
print('del list[4]')
print('list =', list)
