# Shows the list.append(item) method
# mal@tris8VM	2020-11-14 Saturday

print("This program lets you 'add names' to a list and prints them at the end...")
name_list = []

again = 'y'

while again == 'y':
	name = input('Enter a name: ')
	name_list.append(name)
	
	again = input('Do you want to add another name (y = yes)? ')

print('\nHere are the name(s) you entered:')
for name in name_list:
	print(name)
