# Shows the use of list.index(item) method
# This is a slightly modified and improved example
# mal@tris8VM	2020-11-16 Monday

the_list = ['Andy','Bert','Cindy','Dave','Eric','Rupert']
print(the_list)
print()
print('Those are the 6 members of the board.')

modify = input('To modify/add information about the 6 member (press y = yes): ')

while modify.lower() == 'y':
	search = input('Enter the name to modify: ')
	
	try:
		the_index = the_list.index(search)
		
		new_value = input('Enter the new value: ')
		
		the_list[the_index] = new_value
		
		print('Here is the revised list:')
		print(the_list)
		print()
	except ValueError:
		print('The name was not found in the list.')
		print()
	
	modify = input('To modify/add information about the 6 member (press y = yes): ')


