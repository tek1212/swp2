# Shows the min(list) and max(list) function
# mal@tris8VM 2020-11-16 Monday

list = [5,4,3,2,55,44,33,66,77,22]
print('list =', list)
print('min(list) =', min(list))
print('max(list) =', max(list))
