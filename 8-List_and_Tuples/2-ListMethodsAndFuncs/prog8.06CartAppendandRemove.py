# Shows the list.append(item) and list.remove(item) method
# This is an enhanced example to display the list.remove() method
# mal@tris8VM	2020-11-16 Monday

print('This program demonstrates the operations in a typical shopping cart')

cart = []
print('Cart currently contains: ', cart)

op = input('\nWhat operations do you want (a for add) or (r for remove): ')

while op.lower() == 'a' or op.lower() == 'r':
	if op.lower() == 'a':
		item = input('Enter the item you want to add: ')
		cart.append(item)
		print('Cart currently contains: ', cart)
	elif op.lower() == 'r':
		item = input('Enter the item you want to remove: ')
		
		try:
			cart.remove(item)
			print('Cart currently contains: ', cart)
		except ValueError:
			print('The item was not found on the cart.')
			print('Cart currently contains: ', cart)
	
	op = input('\nWhat operations do you want (a for add) or (r for remove): ')
