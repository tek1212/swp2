# Shows a list.sort() and list.reverse() method
# mal@tris8VM	2020-11-16 Monday

a_list = [9,1,0,2,8,6,7,4,5,3]
print('a_list =', a_list)
print('a_list.sort()')
a_list.sort()
print('a_list =', a_list)
a_list.reverse()
print('a_list.reverse()')
print('a_list =', a_list)
print()

b_list = ['beta','alpha','delta','gamma']
print('b_list =', b_list)

b_list.reverse()
print('b_list.reverse()')
print('b_list =', b_list)

print('b_list.sort()')
b_list.sort()
print('b_list =', b_list)

b_list.reverse()
print('b_list.reverse()')
print('b_list =', b_list)

b_list.reverse()
print('b_list.reverse()')
print('b_list =', b_list)
