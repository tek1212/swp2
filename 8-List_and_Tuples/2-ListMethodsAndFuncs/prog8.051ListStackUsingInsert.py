#Shows a LIFO Stack Using Insert
# Also uses List Indexing[] and also Del[index]
#@kiddW7 2022-12-24 Saturday
#@alanW10 2023-04-23 Sunday - fix LILO -> LIFO loL..

print('This program demonstrates a LIFO STACK')

list_stack = []

print('\nThe stack is currently :', list_stack)

cmd = input('\nEnter (a to add) or (p to pop) or anything else to quit: ')

while cmd.lower() == 'a' or cmd.lower() == 'p':
	if cmd.lower()== 'a' :
		item = input('What to put? ')
		list_stack.insert(0,item)
		print('The stack is currently :', list_stack)
	elif cmd.lower() == 'p' :
		try:
			print('Stack popped: ', list_stack[0])
			del list_stack[0]
			print('The stack is currently :', list_stack)
		except IndexError:
			print('There is no item in the stack')
			print('The stack is currently :', list_stack)
			
	cmd = input('\nEnter (a to add) or (p to pop) or anything else to quit: ')
